package printModels

import java.util
import java.util.concurrent.ConcurrentHashMap

import models.Permutation

import scala.collection.mutable.ListBuffer

object PrintMatrice {

  def printM (matrice: Array[Array[ListBuffer[Int]]]): Unit ={

    println()
    for(i<-matrice.indices)
    {
      print(i+" ")
      for(j<-matrice(i).indices){
        if(i == j){
          print(" [x]")
        }else
        if(matrice(i)(j).nonEmpty){
          print(" [")
          for(k<-matrice(i)(j).indices ){
            print( matrice(i)(j)(k))
            if(k != matrice(i)(j).size -1 ){
              print(" ")
            }
          }
          print("] ")
        }else{
          print(" [ ] ")
        }
      }
      println()
    }
  }

  def printPermutation(hashSet:util.HashSet[Permutation],translate: ConcurrentHashMap[Int, String]): Unit ={
    val g:util.Iterator[Permutation] = hashSet.iterator()
    println("*************************************")

    while(g.hasNext) // tant qu'on a un suivant
    {
      val l = g.next()
      println(translate.get(l.x),translate.get(l.y),translate.get(l.z)); // on affiche le suivant
    }
  }


}
