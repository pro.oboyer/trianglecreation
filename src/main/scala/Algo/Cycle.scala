package Algo

import java.util

import scala.collection.mutable.ListBuffer
import util.ArrayList

object Cycle {



  /**
    * Detecter les propriétés partant de X à Y dans un graphe représenter sous forme de matrice
    * @param matrice matrice qui représente le graphe
    * @param X sommet X du graphe
    * @param Y sommet Y du graphe
    * @return la liste des propriétés entre X et Y
    */
  def detectLinkOnIAndJ(matrice: Array[Array[ListBuffer[Long]]], Start:Long, Finish:Long): List[Long] ={
    return matrice(Start.toInt)(Finish.toInt).toList
  }

}
