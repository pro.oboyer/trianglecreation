import models.{CorrespondenceTable, GrapheMatrice, Request}
import Algo.CreateRandomTestFile

object Main extends App  {

  CreateRandomTestFile.create(10000,100,30)
  val t1 = System.nanoTime

  var fileData = "randomTestFile.csv"
  val separatorFileDate = " "
  val fileRequest = "request.txt"
  val separatorFileRequest = " "

  val nameFileData = fileData.substring(0, fileData.length() - 4)
  val table : CorrespondenceTable = GrapheMatrice.createEdge(fileData, separatorFileDate)
  val request = new Request(table, fileRequest, separatorFileRequest, nameFileData)
  request.executeRequest()

  val duration = (System.nanoTime - t1) / 1e9d

  println("duration " +duration)
}



