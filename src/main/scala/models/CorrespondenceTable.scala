package models

import java.io.FileWriter
import java.util.concurrent.ConcurrentHashMap

class CorrespondenceTable(predicatIntToString: ConcurrentHashMap[Int, String],predicatStringToInt: ConcurrentHashMap[String, Int],
                          subjectIntToString: ConcurrentHashMap[Int, String], subjectStringToInt: ConcurrentHashMap[String, Int]) {

  def returnSubjectToString(subj : Int) : String =
    subjectIntToString.get(subj)

  def returnSubjectToInt(subj : String) : Int =
    subjectStringToInt.get(subj)

  def returnPredicatToString(subj : Int) : String =
    predicatIntToString.get(subj)

  def returnPredicatToInt(subj : String) : Int =
    predicatStringToInt.get(subj)

  def saveCorrespondencePredicateTable(name: String) : Unit ={
    val predicatFile = new FileWriter(name + "Predicat.txt",false)
    val keys = predicatIntToString.keys()

    while(keys.hasMoreElements){
      var tmp = keys.nextElement();
      predicatFile.write(tmp + " " + subjectIntToString.get(tmp) + "\n" ) ;
    }
    predicatFile.close()
  }

  def saveCorrespondenceSubjectTable(name: String) ={
    val subjectFile = new FileWriter(name + "subject.txt",false)
    val keys = subjectIntToString.keys()

    while(keys.hasMoreElements){
      var tmp = keys.nextElement();
      subjectFile.write(tmp + " " + subjectIntToString.get(tmp) + "\n" ) ;
    }
    subjectFile.close()

  }
}
