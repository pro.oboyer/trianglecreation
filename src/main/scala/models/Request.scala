package models

import java.io.FileWriter
import scala.util.matching.Regex
import scala.io.Source

class Request(table : CorrespondenceTable, file: String, separatorFile: String, nameFileData: String){
  private val name = file.substring(0, file.length() - 4)

  def executeRequest(): Unit ={
    val tableReq : CorrespondenceTable = GrapheMatrice.createEdge(file, separatorFile)
    val resultFile = new FileWriter(name + "ResultFile.txt",false)
    val bufferedCycle= Source.fromFile(name + "CycleFile.txt")
    val bufferedNoCycle = Source.fromFile(name + "NoCycleFile.txt")
    val lstFile = Array(bufferedCycle, bufferedNoCycle)
    val reg : String = "\\(([0-9]+), \\(([0-9]+),([0-9]+),([0-9]+)\\), \\(([0-9]+),([0-9]+),([0-9]+)\\), \\(([0-9]+),([0-9]+),([0-9]+)\\)\\)"
    val regex : Regex = reg.r
    lstFile.foreach(fi =>
      for (line <- fi.getLines) {
        try {
          for (patternMatch <- regex.findAllMatchIn(line)){
            val p1 = table.returnPredicatToInt(tableReq.returnPredicatToString(patternMatch.group(3).toInt)).toString
            val p2 = table.returnPredicatToInt(tableReq.returnPredicatToString(patternMatch.group(6).toInt)).toString
            val p3 = table.returnPredicatToInt(tableReq.returnPredicatToString(patternMatch.group(9).toInt)).toString
            val tab = Array(p1, p2, p3)

            val triangle = "Triangle found!!! :  " +
              ( "(" + tableReq.returnSubjectToString(patternMatch.group(2).toInt) + ", " + tableReq.returnPredicatToString(patternMatch.group(3).toInt) + ", " + tableReq.returnSubjectToString(patternMatch.group(4).toInt)
            + "), (" + tableReq.returnSubjectToString(patternMatch.group(5).toInt) + ", " + tableReq.returnPredicatToString(patternMatch.group(6).toInt) + ", " + tableReq.returnSubjectToString(patternMatch.group(7).toInt)
            + "), (" + tableReq.returnSubjectToString(patternMatch.group(8).toInt) + ", " + tableReq.returnPredicatToString(patternMatch.group(9).toInt) + ", " + tableReq.returnSubjectToString(patternMatch.group(10).toInt) + ")")

            resultFile.write(triangle + System.getProperty("line.separator"))
            println(triangle)
            if( patternMatch.group(1).toInt == 1){
              resultFile.write(searchTrianglesCycles(tab) + System.getProperty("line.separator"))
            }
            else if( patternMatch.group(1).toInt == 2){
              resultFile.write(searchTrianglesNoCycles(tab) + System.getProperty("line.separator"))
            }
          }
        }
        catch{
          case _: Throwable => println("Error to do the request")
        }
      }
    )
    resultFile.close()
  }

  def searchTrianglesCycles(propArray:Array[String]): String ={
    var str = ""
    var prepRegex = ""
    propArray.foreach(p => {
      prepRegex += p + "|"
    })
    try {

      val reg : String = "\\(([0-9]+), \\(([0-9]+),(" + prepRegex + "),([0-9]+)\\), \\(([0-9]+),(" + prepRegex + "),([0-9]+)\\), \\(([0-9]+),(" + prepRegex + "),([0-9]+)\\)\\)"
      val regex : Regex = reg.r
      val bufferedSource = Source.fromFile(nameFileData + "CycleFile.txt")
      val input: String = bufferedSource.getLines.mkString
      for (patternMatch <- regex.findAllMatchIn(input)){
        val tmp1 = patternMatch.group(3).toInt
        val tmp2 = patternMatch.group(6).toInt
        val tmp3 = patternMatch.group(9).toInt
        val propInt = propArray.map(_.toString.toInt)
        val tmp = Array(tmp1, tmp2, tmp3).sorted
        if(tmp.sameElements(propInt.sorted))
        str = str + (
          "(" + table.returnSubjectToString(patternMatch.group(2).toInt) + ", " + table.returnPredicatToString(tmp1) + ", " + table.returnSubjectToString(patternMatch.group(4).toInt)
          + "), (" + table.returnSubjectToString(patternMatch.group(5).toInt) + ", " + table.returnPredicatToString(tmp2) + ", " + table.returnSubjectToString(patternMatch.group(7).toInt)
            + "), (" + table.returnSubjectToString(patternMatch.group(8).toInt) + ", " + table.returnPredicatToString(tmp3) + ", " + table.returnSubjectToString(patternMatch.group(10).toInt) + ")" + System.getProperty("line.separator"))
        }
    }
    catch{
      case e: Throwable => println("Error to do the request cycle " +e)
    }

    str
  }

  def searchTrianglesNoCycles(propArray:Array[String]): String ={
    var str = ""
    var prepRegex = ""
    propArray.foreach(p => {
      prepRegex += p + "|"
    })
    try {

      val reg : String = "\\(([0-9]+), \\(([0-9]+),(" + prepRegex + "),([0-9]+)\\), \\(([0-9]+),(" + prepRegex + "),([0-9]+)\\), \\(([0-9]+),(" + prepRegex + "),([0-9]+)\\)\\)"
      val regex : Regex = reg.r
      val bufferedSource = Source.fromFile(nameFileData + "NoCycleFile.txt")
      val input: String = bufferedSource.getLines.mkString
      for (patternMatch <- regex.findAllMatchIn(input)){
        val tmp1 = patternMatch.group(3).toInt
        val tmp2 = patternMatch.group(6).toInt
        val tmp3 = patternMatch.group(9).toInt
        str = str + (
          "(" + table.returnSubjectToString(patternMatch.group(2).toInt) + ", " + table.returnPredicatToString(tmp1) + ", " + table.returnSubjectToString(patternMatch.group(4).toInt)
            + "), (" + table.returnSubjectToString(patternMatch.group(5).toInt) + ", " + table.returnPredicatToString(tmp2) + ", " + table.returnSubjectToString(patternMatch.group(7).toInt)
            + "), (" + table.returnSubjectToString(patternMatch.group(8).toInt) + ", " + table.returnPredicatToString(tmp3) + ", " + table.returnSubjectToString(patternMatch.group(10).toInt) + ")"+ System.getProperty("line.separator"))
      }
    }
    catch{
      case _: Throwable => println("Error to do the request no cyle")
    }
    str
  }

}
