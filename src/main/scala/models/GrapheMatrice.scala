package models

import java.io.FileWriter
import java.util
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicInteger

import printModels.PrintMatrice

import scala.collection.mutable.ListBuffer
import scala.io.Source

object GrapheMatrice {
  def createEdge(file: String, separatorFile: String): CorrespondenceTable = {

    val predicatLongToString: ConcurrentHashMap[Int, String] = new ConcurrentHashMap[Int, String]() ;
    val predicatStringToLong: ConcurrentHashMap[String, Int] = new ConcurrentHashMap[String, Int]() ;
    val edges = new ListBuffer[(Int,Int,Int)]()

    val subjectLongToString: ConcurrentHashMap[Int, String] = new ConcurrentHashMap[Int, String]() ;
    val subjectStringToLong: ConcurrentHashMap[String, Int] = new ConcurrentHashMap[String, Int]() ;
    val lastSubjectIndice1:AtomicInteger = new AtomicInteger(0)
    val lastPredicatIndice:AtomicInteger = new AtomicInteger(0)


    val bufferedSource = Source.fromFile(file)
      bufferedSource.getLines().toList.map(
        {
          line =>
            val fields = line.trim.split(separatorFile)
            //modifier le séparateur selon votre fichier
            if(fields.size == 3){
              if(!subjectStringToLong.containsKey(fields(0))){
                val intermediate: Int = lastSubjectIndice1.getAndIncrement()
                subjectStringToLong.put(fields(0),intermediate)
                subjectLongToString.put(intermediate,fields(0))
              }

              if(!subjectStringToLong.containsKey(fields(2))){
                val intermediate: Int = lastSubjectIndice1.getAndIncrement()
                subjectStringToLong.put(fields(2),intermediate)
                subjectLongToString.put(intermediate,fields(2))
              }

              if(!predicatStringToLong.containsKey(fields(1))){
                val intermediate: Int = lastPredicatIndice.getAndIncrement()
                predicatStringToLong.put(fields(1),intermediate)
                predicatLongToString.put(intermediate,fields(1))
              }

              edges += ((subjectStringToLong.get(fields(0)),predicatStringToLong.get(fields(1)),subjectStringToLong.get(fields(2))))
            }
        }
      )
    bufferedSource.close()
    val matrice = new GrapheMatrice(subjectStringToLong.size(), file.substring(0, file.length() - 4))
    matrice.initPropriete(edges)

    val result = matrice.detectTriangleAll()

    //PrintMatrice.printPermutation(result._1,subjectLongToString)
    //PrintMatrice.printPermutation(result._2,subjectLongToString)
    new CorrespondenceTable(predicatLongToString, predicatStringToLong, subjectLongToString, subjectStringToLong)
  }

}
class GrapheMatrice(n:Int, name:String) {

  private val matrice = Array.ofDim[util.HashSet[Int]](n,n)

  def initPropriete (edges:ListBuffer[(Int,Int,Int)]): Unit ={
    var i = 0
    var j = 0
    while(i < n){
      while(j < n){
        matrice(i)(j) = new util.HashSet[Int]

        j+=1
      }
      j = 0
      i+=1
    }
    edges.foreach
    {
      case (x,y,z) =>
        // matrice(x)(z) = y
        if(matrice(x.toInt)(z.toInt) == null){
          matrice(x.toInt)(z.toInt) = new util.HashSet[Int]
        }
        matrice(x.toInt)(z.toInt).add(y)

    }
  }

  def detectTriangleNoCycle()={
    val hashSet  = new util.HashSet[Permutation]() ;

    var i = 0
    var j = 0
    val listJ:util.ArrayList[Int]= new util.ArrayList[Int]();
    for(i<-0 until matrice.size){
      for(j<-0 until matrice(i).size){
        if(matrice(i)(j).size> 0){
          listJ.add(j)
        }
      }
      if(listJ.size > 1){
        if(listJ.size()>0){
          for(k<-0 until (listJ.size)){
            for(l<-0 until (listJ.size)){
              if(listJ.get(l) != listJ.get(k) && listJ.get(l) != i && listJ.get(k) != i){
                if(matrice(listJ.get(l))(listJ.get(k)).size > 0 ){
                  hashSet.add(new Permutation(i,listJ.get(l),listJ.get(k)))

                }
              }
            }
          }
        }
      }
      listJ.removeAll(listJ)
    }
    hashSet
  }

  def detectTriangleCycle(): util.HashSet[Permutation] ={
    val hashSet  = new util.HashSet[Permutation]() ;
    var i = 0
    var i2 = 0
    var i3 = 0
    var i4 = 0
    var j = 0
    var j2 = 0
    var j3 = 0
    matrice.foreach({
      (x) => x.foreach({
        (y) => if(y.size > 0){
          matrice.toList(i).foreach({

            y2 => if(y2.size > 0){
              matrice.toList(i2).foreach({
                y3 => if(y3.size > 0){

                  matrice.toList(i3).foreach({
                    y4 => if(y4.size>0){
                      if(i == i4){
                        hashSet.add(new Permutation(i,i2,i3))

                      }
                    }
                      i4 +=1
                  })
                  i4=0
                  j3+=1
                }
                  i3 += 1
                  j3=0
              })
              i3 = 0
              j2+=1
            }
              i2+=1
              j2=0
          })

          i2=0
        }

          j+=1
      })
        j=0
        i+=1

    })

    hashSet
  }


  def detectTriangleAll(): ( util.HashSet[Permutation], util.HashSet[Permutation]) ={
    val hashSet  = new util.HashSet[Permutation]() ;
    val hashSetNo  = new util.HashSet[Permutation]() ;
    val listJ:util.ArrayList[Int]= new util.ArrayList[Int]();
    val listPrep:util.ArrayList[Int]= new util.ArrayList[Int]();
    val cycleFile = new FileWriter("cycleFile.txt",true)
    var i = 0
    var i2 = 0
    var i3 = 0
    var i4 = 0
    var j = 0
    var j2 = 0
    var j3 = 0


    for(i<-0 until matrice.size) {
      for(i2<-0 until matrice(i).size){
        if(matrice(i)(i2).size> 0) {
          listJ.add(i2)
          for(i3<-0 until matrice(i2).size){
            if(matrice(i2)(i3).size() > 0){
                if(matrice(i3)(i).size() > 0) {
                    hashSet.add(new Permutation(i,i2,i3))
                }
            }
          }
        }
      }
      if(listJ.size > 1){
        for(k<-0 until (listJ.size)){
          for(l<-k until (listJ.size)){
            if(listJ.get(l) != listJ.get(k) && listJ.get(l) != i && listJ.get(k) != i){
              if(matrice(listJ.get(l))(listJ.get(k)).size > 0){
                hashSetNo.add(new Permutation(i,listJ.get(l),listJ.get(k)))
              }
              if ( matrice(listJ.get(k))(listJ.get(l)).size > 0){
                hashSetNo.add(new Permutation(i,listJ.get(k),listJ.get(l)))
              }
            }
          }
        }
      }
      listJ.removeAll(listJ)
    }
    writeInFileCycle(hashSet)
    writeInFileNoCycle(hashSetNo)
    (hashSet,hashSetNo)
  }



  def getMatrice = matrice;

  def writeInFileCycle(hashSet: util.HashSet[models.Permutation]): Unit ={
    val array = hashSet.toArray()
    println("writeInFileCycle size array" + array.size+" ----- "+name + "CycleFile.txt")
    var i = 0
    val cycleFile = new FileWriter(name + "CycleFile.txt",false)
    try {
      array.foreach(element => {
        val permut = element.asInstanceOf[models.Permutation]
        matrice(permut.x)(permut.y).toArray().foreach(prop1=> {
          matrice(permut.y)(permut.z).toArray().foreach(prop2 => {
            matrice(permut.z)(permut.x).toArray().foreach(prop3 =>{
              val triangleCycle = "(1, ("+permut.x+","+prop1+","+permut.y+"), ("+permut.y+","+prop2+","+permut.z+"), ("+permut.z+","+prop3+","+permut.x+"))\n"
              cycleFile.write(triangleCycle)
              i=i+1
              cycleFile.write(System.getProperty("line.separator"));
            })
          })
        })
      })
    } finally cycleFile.close()

    println("writeInFileCycle number write" +i)
  }

  def writeInFileNoCycle(hashSet: util.HashSet[models.Permutation]) : Unit = {
    val array = hashSet.toArray()
    println("writeInFileNoCycle size array" + array.size + " ---- "+name + "NoCycleFile.txt")
    var i = 0
    val noCycleFile = new FileWriter(name + "NoCycleFile.txt",false)
    try {
      array.foreach(element => {
        val permut = element.asInstanceOf[models.Permutation]
        matrice(permut.x)(permut.y).toArray().foreach(prop1=> {
          matrice(permut.x)(permut.z).toArray().foreach(prop2 => {
            matrice(permut.y)(permut.z).toArray().foreach(prop3 =>{
              val triangleNoCycle = "(2, ("+permut.x+","+prop1+","+permut.y+"), ("+permut.x+","+prop2+","+permut.z+"), ("+permut.y+","+prop3+","+permut.z+"))"
              noCycleFile.write(triangleNoCycle)
              noCycleFile.write(System.getProperty("line.separator"));
              i=i+1
            })
          })
        })
      })
    } finally noCycleFile.close()
    println("writeInFileNoCycle number write" +i)
  }


}

