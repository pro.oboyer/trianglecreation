package models

class Permutation(val x:Int, val y:Int,val z:Int) {

  override def equals(that: Any): Boolean =
    that match {
      case that: Permutation =>
        (((this.x == that.x ) && (this.y == that.y ) && (this.z == that.z )) ||
         ((this.x == that.z ) && (this.y == that.x ) && (this.z == that.y )) ||
         ((this.x == that.y ) && (this.y == that.z ) && (this.z == that.x ))
         ) && that.isInstanceOf[Permutation]

      case _ => false
    }

  override def hashCode: Int = {
    x+y+z

  }

  override def toString: String ={
    "("+x+","+y+","+z+")"
  }

}
